import { Avatar, AvatarGroup, Box, ImageList, ImageListItem, Typography } from "@mui/material";
import React from "react";

const Rightbar = () => {
    return (
        <Box
            flex={2}
            p={2}
            sx={{ display: { xs: "none", sm: "block" } }}
        >
            <Box position={"fixed"} width={300}>
                <Typography variant="h6" fontWeight={100} mt={2} mb={2}>
                    Online Friends
                </Typography>
                <AvatarGroup max={7}>
                    <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg" />
                    <Avatar alt="Travis Howard" src="https://material-ui.com/static/images/avatar/2.jpg" />
                    <Avatar alt="Cindy Baker" src="https://material-ui.com/static/images/avatar/3.jpg" />
                    <Avatar alt="Agnes Walker" src="https://material-ui.com/static/images/avatar/4.jpg" />
                    <Avatar alt="Trevor Henderson" src="https://material-ui.com/static/images/avatar/5.jpg" />
                    <Avatar alt="Billy Madison" src="" />
                    <Avatar alt="Will Smith" src="https://material-ui.com/static/images/avatar/6.jpg" />
                    <Avatar alt="Jeremy Irons" src="https://material-ui.com/static/images/avatar/7.jpg" />
                    <Avatar alt="Mohamed Salah" src="https://material-ui.com/static/images/avatar/8.jpg" />
                </AvatarGroup>
                <Typography variant="h6" fontWeight={100}>
                    Latest Posts
                </Typography>
                <ImageList cols={3} rowHeight={100} gap={5}>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/breakfast.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/burgers.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/camera.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/hats.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/honey.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/mushroom.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/bike.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/morning.jpg" />
                    </ImageListItem>
                    <ImageListItem >
                        <img src="https://material-ui.com/static/images/image-list/breakfast.jpg" />
                    </ImageListItem>
                </ImageList>
            </Box>
        </Box>
    )
}

export default Rightbar;