import { Favorite, FavoriteBorder, MoreVert, Share } from "@mui/icons-material";
import { Avatar, Card, CardActions, CardContent, CardHeader, CardMedia, Checkbox, IconButton, Typography } from "@mui/material";
import React from "react";

const Post = () => {
    return (
        <div>
            <Card sx={{margin:5}}>
                <CardHeader
                    avatar={
                        <Avatar sx={{ bgcolor: "red" }} aria-label="recipe">
                            W
                        </Avatar>
                    }
                    action={
                        <IconButton aria-label="settings">
                            <MoreVert />
                        </IconButton>
                    }
                    title="Wesley Shaw"
                    subheader="February 20th, 2024"
                />
                <CardMedia
                    component="img"
                    height="20%"
                    image="https://media.cntraveler.com/photos/60341fbad7bd3b27823c9db2/16:9/w_960,c_limit/Tokyo-2021-GettyImages-1208124099.jpg"
                    alt="Paella dish"
                />
                <CardContent>
                    <Typography variant="body2" color="text.secondary">
                        A picture of Mount Fiji taken from my trip to Japan.
                    </Typography>
                </CardContent>
                <CardActions disableSpacing>
                    <Checkbox icon={<FavoriteBorder />} checkedIcon={<Favorite sx={{ color: "red" }} />} />
                    <IconButton aria-label="share">
                        <Share />
                    </IconButton>
                </CardActions>
            </Card>
        </div>
    )
}

export default Post;